﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using TBroker.Client.Sender;

namespace Sender.HostedServices
{
    public class NewDataBackgroundService : BackgroundService
    {
        private readonly IDataQueue _dataQueue;

        public NewDataBackgroundService(IDataQueue dataQueue)
        {
            _dataQueue = dataQueue;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var rand = new Random();

            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromSeconds(1));

                if (!_dataQueue.HasData)
                {
                    var number = rand.Next(1, 10000);
                    Console.WriteLine("NewNumbers: " + number);

                    _dataQueue.Add("NewNumbers", new { Number = number });
                }
            }
        }
    }
}
