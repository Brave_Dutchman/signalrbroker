﻿using System;
using Reciever.Models;
using TBroker.Client.Reciever;

namespace Reciever
{
    public class TestContainer : IRecieverContainer
    {
        public void Initialize(RecieverServiceBuilder builder)
        {
            builder.On<NumberModel>("NewNumbers", Number);
        }

        private void Number(NumberModel obj)
        {
            Console.WriteLine("NewNumbers: " + obj.Number);
        }
    }
}
