﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace TBroker.Client
{
    public static class ServiceProviderExtentions
    {
        /// <summary>
        /// Add an <see cref="IHostedService"/> registration for the given type.
        /// </summary>
        /// <typeparam name="THostedService">An <see cref="IHostedService"/> to register.</typeparam>
        /// <param name="services">The <see cref="IServiceCollection"/> to register with.</param>
        /// <returns>The original <see cref="IServiceCollection"/>.</returns>
        public static IServiceCollection AddHostedService<THostedService>(this IServiceCollection services, Func<IServiceProvider, THostedService> implementationFactory)
            where THostedService : class, IHostedService
                => services.AddTransient<IHostedService, THostedService>(implementationFactory);

        public static IServiceCollection AddSharedSingleton<TService, TImplementation>(this IServiceCollection services)
            where TService : class
            where TImplementation : class, TService
        {
            services.TryAddSingleton<TImplementation>();
            services.AddSingleton<TService, TImplementation>(serviceProvider => serviceProvider.GetRequiredService<TImplementation>());

            return services;
        }

        public static IServiceCollection AddSharedScoped<TService, TImplementation>(this IServiceCollection services)
            where TService : class
            where TImplementation : class, TService
        {
            services.TryAddScoped<TImplementation>();
            services.AddScoped<TService, TImplementation>(serviceProvider => serviceProvider.GetRequiredService<TImplementation>());

            return services;
        }
    }
}
