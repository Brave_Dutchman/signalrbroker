﻿using System.Threading.Tasks;

namespace TBroker.Client
{
    public delegate Task NotifierHandler<T>(T args);
    public delegate Task NotifierHandler();
}
