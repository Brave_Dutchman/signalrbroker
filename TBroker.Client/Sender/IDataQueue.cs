﻿using System.Threading.Tasks;

namespace TBroker.Client.Sender
{
    public interface IDataQueue
    {
        int Count { get; }

        bool HasData { get; }

        void Add(string group, object data);
        Task<(string group, object data)> Dequeue();
    }
}