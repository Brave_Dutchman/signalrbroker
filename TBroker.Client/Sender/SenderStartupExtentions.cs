﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using TBroker.Client.Sender.Internal;

namespace TBroker.Client.Sender
{
    public static class SenderStartupExtentions
    {
        public static ITBrokerSenderBuilder AddSender(this IServiceCollection services)
        {
            services.TryAddSingleton<IDataQueue, DataQueue>();

            services.AddHostedService((serviceProvider) =>
            {
                var connection = serviceProvider.GetRequiredService<BrokerConnection>();
                var service = new SenderBackgroundService(connection, serviceProvider.GetRequiredService<IDataQueue>(), serviceProvider.GetRequiredService<SenderOptions>());

                return service;
            });

            return new TBrokerSenderBuilder(services);
        }
    }
}
