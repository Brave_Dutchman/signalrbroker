﻿using Microsoft.Extensions.DependencyInjection;

namespace TBroker.Client.Sender
{
    public interface ITBrokerSenderBuilder
    {
        IServiceCollection Services { get; }
    }
}
