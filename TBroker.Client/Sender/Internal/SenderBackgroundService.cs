﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace TBroker.Client.Sender.Internal
{
    internal class SenderBackgroundService : BackgroundService
    {
        private readonly BrokerConnection _connection;
        private CancellationToken _stoppingToken;

        private readonly IDataQueue _dataQueue;
        private readonly SenderOptions _options;
        private bool _isClosing;

        public SenderBackgroundService(BrokerConnection connection, IDataQueue dataQueue, SenderOptions options)
        {
            _connection = connection;
            _dataQueue = dataQueue;
            _options = options;

            _connection.IsConnectedChanged += Connection_IsConnectedChanged;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _stoppingToken = stoppingToken;

            await _connection.StartAsync(stoppingToken);

            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1, stoppingToken);

                while (_connection.IsConnected && !stoppingToken.IsCancellationRequested)
                {
                    var (group, data) = await _dataQueue.Dequeue();
                    if (group != null)
                    {
                        try
                        {
                            await _connection.SendToBroker(group, data);
                        }
                        catch (System.Exception)
                        {
                            _dataQueue.Add(group, data);
                        }
                    }
                }
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _isClosing = true;
            var disposeConnection = _connection.DisposeAsync();
            await Task.WhenAll(disposeConnection, base.StopAsync(cancellationToken));
        }

        private async Task Connection_IsConnectedChanged(ConnectionChangeReason reason)
        {
            if (_isClosing)
            {
                return;
            }

            switch (reason)
            {
                case ConnectionChangeReason.Connected:
                    break;
                case ConnectionChangeReason.ConnectingFailed:
                    await Task.Delay(_options.ReconnectDelayInMilliseconds, _stoppingToken);
                    await _connection.StartAsync(_stoppingToken);
                    break;
                case ConnectionChangeReason.Disconnected:
                    await _connection.StartAsync(_stoppingToken);
                    break;
                default:
                    break;
            }
        }
    }
}
