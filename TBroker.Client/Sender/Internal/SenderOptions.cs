﻿using System;

namespace TBroker.Client.Sender.Internal
{
    public class SenderOptions
    {
        public SenderOptions()
        {
            ReconnectDelayInMilliseconds = (int) TimeSpan.FromSeconds(10).TotalMilliseconds;
        }

        public int ReconnectDelayInMilliseconds { get; set; }
    }
}
