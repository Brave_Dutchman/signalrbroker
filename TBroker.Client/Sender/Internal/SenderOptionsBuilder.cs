﻿using System;

namespace TBroker.Client.Sender.Internal
{
    public class SenderOptionsBuilder
    {
        private readonly SenderOptions _options = new SenderOptions();

        public SenderOptionsBuilder WithReconnectDelay(int milliseconds)
        {
            _options.ReconnectDelayInMilliseconds = milliseconds;
            return this;
        }

        public SenderOptionsBuilder WithReconnectDelay(TimeSpan timespan)
        {
            _options.ReconnectDelayInMilliseconds = (int) timespan.TotalMilliseconds;
            return this;
        }

        internal SenderOptions Build()
        {
            return _options;
        }
    }
}
