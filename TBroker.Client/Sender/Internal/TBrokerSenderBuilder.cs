﻿using Microsoft.Extensions.DependencyInjection;

namespace TBroker.Client.Sender.Internal
{
    internal class TBrokerSenderBuilder : ITBrokerSenderBuilder
    {
        public TBrokerSenderBuilder(IServiceCollection services)
        {
            Services = services;
        }

        public IServiceCollection Services { get; }
    }
}
