﻿using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace TBroker.Client.Sender.Internal
{
    internal class DataQueue : IDataQueue
    {
        private readonly ConcurrentQueue<(string group, object data)> _queue;

        private SemaphoreSlim _semaphore = new SemaphoreSlim(0);

        public DataQueue()
        {
            _queue = new ConcurrentQueue<(string group, object data)>();
        }

        public void Add(string group, object data)
        {
            _queue.Enqueue((group, data));

            _semaphore.Release();
        }

        public int Count => _queue.Count;

        public bool HasData => _queue.Count > 0;

        public async Task<(string group, object data)> Dequeue()
        {
            await _semaphore.WaitAsync();

            _queue.TryDequeue(out var result);

            return result;
        }
    }
}
