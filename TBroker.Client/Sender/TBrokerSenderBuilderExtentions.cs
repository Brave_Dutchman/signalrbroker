﻿using System;
using Microsoft.Extensions.DependencyInjection;
using TBroker.Client.Sender.Internal;

namespace TBroker.Client.Sender
{
    public static class TBrokerSenderBuilderExtentions
    {
        public static ITBrokerSenderBuilder WithOptions(this ITBrokerSenderBuilder builder, Action<SenderOptionsBuilder> configure)
        {
            builder.Services.AddSingleton(_ =>
            {
                var optionsBuilder = new SenderOptionsBuilder();
                configure.Invoke(optionsBuilder);
                return optionsBuilder.Build();
            });

            return builder;
        }
    }
}
