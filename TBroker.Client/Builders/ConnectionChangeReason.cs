﻿namespace TBroker.Client
{
    public enum ConnectionChangeReason
    {
        Connected,
        ConnectingFailed,
        Disconnected
    }
}
