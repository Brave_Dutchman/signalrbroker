﻿using System;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;

namespace TBroker.Builders
{
    public class TBrokerHubConnectionBuilder : IHubConnectionBuilder
    {
        private readonly HubConnectionBuilder _inner = new HubConnectionBuilder();
        private readonly Action<TBrokerHubConnectionBuilder> _builderMethod;

        public TBrokerHubConnectionBuilder(Action<TBrokerHubConnectionBuilder> builderMethod)
        {
            _builderMethod = builderMethod;
        }

        public IServiceCollection Services => _inner.Services;

        public HubConnection Build()
        {
            _builderMethod(this);
            return _inner.Build();
        }
    }
}
