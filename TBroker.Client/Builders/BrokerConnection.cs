﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace TBroker.Client
{
    public class BrokerConnection
    {
        private bool _isConnected;

        public BrokerConnection(HubConnection connection)
        {
            _Inner = connection;
            _Inner.Closed += SignalRConnectionClosed;
        }

        public event NotifierHandler<ConnectionChangeReason> IsConnectedChanged;

        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }
            set
            {
                bool _oldValue = _isConnected;
                _isConnected = value;

                ConnectionChangeReason? reason = null;
                if (_oldValue == false && _oldValue == _isConnected)
                {
                    reason = ConnectionChangeReason.ConnectingFailed;
                }
                else if (_oldValue != _isConnected)
                {
                    if (_oldValue) //  Was connected
                    {
                        reason = ConnectionChangeReason.Disconnected;
                    }
                    else
                    {
                        reason = ConnectionChangeReason.Connected;
                    }
                }

                if (reason.HasValue)
                {
                    IsConnectedChanged?.Invoke(reason.GetValueOrDefault());
                }
            }
        }

        internal protected HubConnection _Inner { get; }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                await _Inner.StartAsync(cancellationToken);

                Console.WriteLine("Connected succesfully");

                IsConnected = true;
            }
            catch (Exception)
            {
                // TODO logging
                IsConnected = false;
            }
        }

        public async Task SendToBroker(string group, object data)
        {
            await _Inner.InvokeAsync("Distrubute", group, data);
        }

        public async Task JoinGroupAsync(string groupName)
        {
            await _Inner.InvokeAsync(nameof(JoinGroupAsync), groupName);
        }

        public async Task LeaveGroupAsync(string groupName)
        {
            await _Inner.InvokeAsync(nameof(LeaveGroupAsync), groupName);
        }

        public async Task DisposeAsync()
        {
            IsConnected = false;
            await _Inner.DisposeAsync();
        }

        private Task SignalRConnectionClosed(Exception exception)
        {
            IsConnected = false;
            return Task.CompletedTask;
        }
    }
}
