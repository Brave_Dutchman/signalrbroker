﻿using System;
using Microsoft.Extensions.DependencyInjection;
using TBroker.Builders;

namespace TBroker.Client
{
    public static class SharedStartupExtentions
    {
        public static IServiceCollection AddBrokerConnection(this IServiceCollection services, Action<TBrokerHubConnectionBuilder> intiializer)
        {
            return services.AddSingleton(_ =>
            {
                var hubConnection = new TBrokerHubConnectionBuilder(intiializer).Build();

                return new BrokerConnection(hubConnection);
            });
        }
    }
}
