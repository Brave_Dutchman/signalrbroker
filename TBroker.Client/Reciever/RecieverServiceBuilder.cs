﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR.Client;
using TBroker.Client.Reciever.Internal;

namespace TBroker.Client.Reciever
{
    public class RecieverServiceBuilder
    {
        private readonly RecieverOptions _options;
        private readonly HubConnection _connection;
        private readonly BrokerConnection _brokerConnection;
        private readonly List<string> _groupNames;

        public RecieverServiceBuilder(BrokerConnection brokerConnection, RecieverOptions options)
        {
            _brokerConnection = brokerConnection;
            _options = options;
            _groupNames = new List<string>();

            _connection = _brokerConnection._Inner;
        }

        public RecieverServiceBuilder On(string methodName, Action handler)
        {
            _connection.On(methodName, handler);
            _groupNames.Add(methodName);
            return this;
        }

        public RecieverServiceBuilder On<T1, T2, T3, T4, T5, T6, T7, T8>(string methodName, Action<T1, T2, T3, T4, T5, T6, T7, T8> handler)
        {
            _connection.On(methodName, handler);
            _groupNames.Add(methodName);
            return this;
        }

        public RecieverServiceBuilder On<T1, T2, T3, T4, T5, T6, T7>(string methodName, Action<T1, T2, T3, T4, T5, T6, T7> handler)
        {
            _connection.On(methodName, handler);
            _groupNames.Add(methodName);
            return this;
        }

        public RecieverServiceBuilder On<T1, T2, T3, T4, T5, T6>(string methodName, Action<T1, T2, T3, T4, T5, T6> handler)
        {
            _connection.On(methodName, handler);
            _groupNames.Add(methodName);
            return this;
        }

        public RecieverServiceBuilder On<T1, T2, T3, T4, T5>(string methodName, Action<T1, T2, T3, T4, T5> handler)
        {
            _connection.On(methodName, handler);
            _groupNames.Add(methodName);
            return this;
        }

        public RecieverServiceBuilder On<T1, T2, T3, T4>(string methodName, Action<T1, T2, T3, T4> handler)
        {
            _connection.On(methodName, handler);
            _groupNames.Add(methodName);
            return this;
        }

        public RecieverServiceBuilder On<T1, T2, T3>(string methodName, Action<T1, T2, T3> handler)
        {
            _connection.On(methodName, handler);
            _groupNames.Add(methodName);
            return this;
        }

        public RecieverServiceBuilder On<T1, T2>(string methodName, Action<T1, T2> handler)
        {
            _connection.On(methodName, handler);
            _groupNames.Add(methodName);
            return this;
        }

        public RecieverServiceBuilder On<T1>(string methodName, Action<T1> handler)
        {
            _connection.On(methodName, handler);
            _groupNames.Add(methodName);
            return this;
        }

        internal RecieverBackgroundService Build()
        {
            return new RecieverBackgroundService(_brokerConnection, _groupNames, _options);
        }
    }
}
