﻿using Microsoft.Extensions.DependencyInjection;

namespace TBroker.Client.Reciever.Internal
{
    internal class TBrokerRecieverBuilder : ITBrokerRecieverBuilder
    {
        public TBrokerRecieverBuilder(IServiceCollection services)
        {
            Services = services;
        }

        public IServiceCollection Services { get; }
    }
}
