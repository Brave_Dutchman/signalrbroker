﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace TBroker.Client.Reciever.Internal
{
    internal class RecieverBackgroundService : BackgroundService
    {
        private readonly BrokerConnection _connection;
        private readonly RecieverOptions _options;
        private readonly IReadOnlyList<string> _groups;

        private CancellationToken _stoppingToken;
        private bool _isClosing;

        public RecieverBackgroundService(BrokerConnection connection, IReadOnlyList<string> groups,  RecieverOptions options)
        {
            _connection = connection;
            _options = options;
            _groups = groups;

            _connection.IsConnectedChanged += Connection_IsConnectedChanged;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _stoppingToken = stoppingToken;

            await _connection.StartAsync(stoppingToken);

            await Task.Delay(Timeout.Infinite, stoppingToken);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _isClosing = true;
            var disposeConnection = _connection.DisposeAsync();
            await Task.WhenAll(disposeConnection, base.StopAsync(cancellationToken));
        }

        private async Task Connection_IsConnectedChanged(ConnectionChangeReason reason)
        {
            if (_isClosing)
            {
                return;
            }

            switch (reason)
            {
                case ConnectionChangeReason.Connected:
                    await StartListening();
                    break;
                case ConnectionChangeReason.ConnectingFailed:
                    await Task.Delay(_options.ReconnectDelayInMilliseconds, _stoppingToken);
                    await _connection.StartAsync(_stoppingToken);
                    break;
                case ConnectionChangeReason.Disconnected:
                    await _connection.StartAsync(_stoppingToken);
                    break;
                default:
                    break;
            }
        }

        private async Task StartListening()
        {
            Console.WriteLine("Starting listening");

            foreach (var groupName in _groups)
            {
                await _connection.JoinGroupAsync(groupName);
                Console.WriteLine($"Listening for updates from group: {groupName}");
            }
        }
    }
}
