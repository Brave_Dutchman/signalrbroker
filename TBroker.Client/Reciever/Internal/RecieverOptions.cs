﻿using System;

namespace TBroker.Client.Reciever.Internal
{
    public class RecieverOptions
    {
        public RecieverOptions()
        {
            ReconnectDelayInMilliseconds = (int) TimeSpan.FromSeconds(10).TotalMilliseconds;
        }

        public int ReconnectDelayInMilliseconds { get; set; }
    }
}
