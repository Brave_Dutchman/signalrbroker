﻿using System;

namespace TBroker.Client.Reciever.Internal
{
    public class RecieverOptionsBuilder
    {
        private readonly RecieverOptions _options = new RecieverOptions();

        public RecieverOptionsBuilder WithReconnectDelay(int milliseconds)
        {
            _options.ReconnectDelayInMilliseconds = milliseconds;
            return this;
        }

        public RecieverOptionsBuilder WithReconnectDelay(TimeSpan timespan)
        {
            _options.ReconnectDelayInMilliseconds = (int) timespan.TotalMilliseconds;
            return this;
        }

        internal RecieverOptions Build()
        {
            return _options;
        }
    }
}
