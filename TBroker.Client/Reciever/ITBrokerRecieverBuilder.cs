﻿using Microsoft.Extensions.DependencyInjection;

namespace TBroker.Client.Reciever
{
    public interface ITBrokerRecieverBuilder
    {
        IServiceCollection Services { get; }
    }
}
