﻿using TBroker.Client.Reciever.Internal;

namespace TBroker.Client.Reciever
{
    public interface IRecieverContainer
    {
        void Initialize(RecieverServiceBuilder builder);
    }
}
