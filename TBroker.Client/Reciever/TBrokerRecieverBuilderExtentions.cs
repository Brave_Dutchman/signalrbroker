﻿using System;
using Microsoft.Extensions.DependencyInjection;
using TBroker.Client.Reciever.Internal;

namespace TBroker.Client.Reciever
{
    public static class TBrokerRecieverBuilderExtentions
    {
        public static ITBrokerRecieverBuilder WithOptions(this ITBrokerRecieverBuilder builder, Action<RecieverOptionsBuilder> configure)
        {
            builder.Services.AddSingleton(_ =>
            {
                var optionsBuilder = new RecieverOptionsBuilder();
                configure.Invoke(optionsBuilder);
                return optionsBuilder.Build();
            });

            return builder;
        }
    }
}
