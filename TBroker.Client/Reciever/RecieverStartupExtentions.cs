﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using TBroker.Client.Reciever.Internal;

namespace TBroker.Client.Reciever
{
    public static class RecieverStartupExtentions
    {
        public static ITBrokerRecieverBuilder AddReciever(this IServiceCollection services, Action<RecieverServiceBuilder> configure)
        {
            services.AddHostedService(serviceProvider =>
            {
                var brokerConnection = serviceProvider.GetRequiredService<BrokerConnection>();
                var options = serviceProvider.GetRequiredService<RecieverOptions>();
                
                var builder = new RecieverServiceBuilder(brokerConnection, options);

                configure.Invoke(builder);

                return builder.Build();
            });

            return new TBrokerRecieverBuilder(services);
        }

        public static ITBrokerRecieverBuilder AddReciever<TContainer>(this IServiceCollection services)
            where TContainer : class, IRecieverContainer
        {
            services.TryAddSingleton<RecieverServiceBuilder>();
            services.TryAddSingleton<TContainer>();
            
            services.AddHostedService(serviceProvider =>
            {
                var builder = serviceProvider.GetRequiredService<RecieverServiceBuilder>();
                var container = serviceProvider.GetRequiredService<TContainer>();
                container.Initialize(builder);

                return builder.Build();
            });

            return new TBrokerRecieverBuilder(services);
        }
    }
}
