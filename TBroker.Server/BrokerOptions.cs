﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;

namespace TBroker.Server
{
    // HandshakeTimeout and KeepAliveInterval are set to null here to help identify when
    // local hub options have been set. Global default values are set in HubOptionsSetup.
    // SupportedProtocols being null is the true default value, and it represents support
    // for all available protocols.
    public class BrokerOptions
    {
        private readonly HubOptions _options;

        public BrokerOptions(HubOptions options)
        {
            _options = options;
        }
        
        /// <summary>
        /// Gets or sets the interval used by the server to timeout incoming handshake requests by clients.
        /// </summary>
        public TimeSpan? HandshakeTimeout { get; set; } = null;

        /// <summary>
        /// Gets or sets the interval used by the server to send keep alive pings to connected clients.
        /// </summary>
        public TimeSpan? KeepAliveInterval
        {
            get
            {
                return _options.KeepAliveInterval;
            }
            set
            {
                _options.KeepAliveInterval = value;
            }
        }

        /// <summary>
        /// Gets or sets a collection of supported hub protocol names.
        /// </summary>
        public IList<string> SupportedProtocols
        {
            get
            {
                return _options.SupportedProtocols;
            }
            set
            {
                _options.SupportedProtocols = value;
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether detailed error messages are sent to the client.
        /// Detailed error messages include details from exceptions thrown on the server.
        /// </summary>
        public bool? EnableDetailedErrors
        {
            get
            {
                return _options.EnableDetailedErrors;
            }
            set
            {
                _options.EnableDetailedErrors = value;
            }
        }
    }
}
