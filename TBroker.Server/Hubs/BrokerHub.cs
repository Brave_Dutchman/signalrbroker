﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace TBroker.Server.Hubs
{
    public class BrokerHub : Hub
    {
        private readonly ILogger<BrokerHub> _logger;

        public BrokerHub(ILogger<BrokerHub> logger)
        {
            _logger = logger;
        }


        public override async Task OnConnectedAsync()
        {
            _logger.LogInformation("Connected client {id}", Context.ConnectionId);

            await base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _logger.LogInformation("Disconnected client {id}", Context.ConnectionId);

            return base.OnDisconnectedAsync(exception);
        }

        public async Task JoinGroupAsync(string group)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, group);
        }

        public async Task Distrubute(string group, object data)
        {
            _logger.LogInformation("Distrubute for group:'{group}' by id:'{id}'", group, Context.ConnectionId);

            await Clients.Group(group).SendAsync(group, data);
        }
    }
}
