﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using TBroker.Server.Hubs;

namespace TBroker.Server
{
    public static class StartupExtentions
    {
        public static IServiceCollection AddTBroker(this IServiceCollection services)
        {
            services.AddSignalR();
            return services;
        }

        public static IServiceCollection AddTBroker(this IServiceCollection services, Action<BrokerOptions> configure)
        {
            void internalConfigure(HubOptions hubOptions)
            {
                BrokerOptions brokerOptions = new BrokerOptions(hubOptions);
                configure(brokerOptions);
            }

            services.AddSignalR(internalConfigure);
            return services;
        }

        public static IApplicationBuilder UseTBroker(this IApplicationBuilder app)
        {
            return app.UseSignalR(x => x.MapHub<BrokerHub>("/broker"));
        }
    }
}
